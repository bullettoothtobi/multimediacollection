<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20141114091103 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        
        $this->addSql('CREATE TABLE Category (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, createdOn DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Entity (id INT AUTO_INCREMENT NOT NULL, category_id INT DEFAULT NULL, medium_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, createdOn DATETIME NOT NULL, INDEX IDX_984415E12469DE2 (category_id), INDEX IDX_984415EE252B6A5 (medium_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Genre (id INT AUTO_INCREMENT NOT NULL, entity_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, createdOn DATETIME NOT NULL, INDEX IDX_42911CFC81257D5D (entity_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Medium (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, createdOn DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE Entity ADD CONSTRAINT FK_984415E12469DE2 FOREIGN KEY (category_id) REFERENCES Category (id)');
        $this->addSql('ALTER TABLE Entity ADD CONSTRAINT FK_984415EE252B6A5 FOREIGN KEY (medium_id) REFERENCES Medium (id)');
        $this->addSql('ALTER TABLE Genre ADD CONSTRAINT FK_42911CFC81257D5D FOREIGN KEY (entity_id) REFERENCES Entity (id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        
        $this->addSql('ALTER TABLE Entity DROP FOREIGN KEY FK_984415E12469DE2');
        $this->addSql('ALTER TABLE Genre DROP FOREIGN KEY FK_42911CFC81257D5D');
        $this->addSql('ALTER TABLE Entity DROP FOREIGN KEY FK_984415EE252B6A5');
        $this->addSql('DROP TABLE Category');
        $this->addSql('DROP TABLE Entity');
        $this->addSql('DROP TABLE Genre');
        $this->addSql('DROP TABLE Medium');
    }
}
