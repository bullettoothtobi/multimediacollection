<?php

namespace MultimediaCollection\WebAppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class MainpageController extends Controller
{
    /**
     * @Route("/", name="_mainpage_index")
     */
    public function indexAction()
    {
        return $this->render('MultimediaCollectionWebAppBundle:Mainpage:index.html.twig');
    }
}
